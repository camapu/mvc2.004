package net.canos.spring.webapp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import net.canos.spring.webapp.Person.Gender;

@Controller
public class ThymeleafController {

	Logger log = Logger.getLogger(this.getClass());
	
	//http://localhost:8080/mvc2/
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model){
		log.info("home");
		model.addAttribute("name","David Canós");
		model.addAttribute("uname","<i>David</i>");
		model.addAttribute("price",1850);
		model.addAttribute("price2",50);
		model.addAttribute("date",new Date());
		
		List<String> names = new ArrayList<>();
		names.add("María");
		names.add("Juan Miguel");
		model.addAttribute("names",names);
		
		Person p = new Person();
		p.setName("Ayr");
		p.setSurName("Mackenroe");
		p.setGender(null);
		model.addAttribute("person_ungendered",p);
		
		Person p2 = new Person();
		p2.setName("Pepe Luis");
		p2.setSurName("Romerales");
		p2.setGender(Gender.MALE);
		p2.setWeight(69);
		model.addAttribute("person_gendered",p2);
		
		Person p3 = new Person();
		model.addAttribute("person",p3);
		
		return "welcome";
	}
	
	
	//http://localhost:8080/mvc2/param?name=lol
	@RequestMapping(value = "/param", method = RequestMethod.GET)
	public String param(@RequestParam(required=false,defaultValue="") String name){
		return "name:"+name;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(Person person, String other, String var){
		log.info(person.getName()+" "+person.getSurName()+" "+other+" "+var);
		return "welcome";
	}
	
	
}
